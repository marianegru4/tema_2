function addTokens(input, tokens){
    
    //verific daca tipul input-ului este string
    if (!(typeof input == 'string'))
    {
        throw 'Invalid input';
    }
    
    //verific daca lungimea input-ului este minim 6
    if(input.length < 6)
    {
        throw 'Input should have at least 6 characters';
    }

    //verific daca array-ul de tokeni are formatul cerut
    //folosesc functia implementata mai jos
    if (!verifyTokenFormat(tokens))
    {
        throw 'Invalid array format';
    }

    //verific daca trebuie sa adaug informatii din array-ul de tokeni in rezultatul final
    if (input.includes('...',0))
    {
        for(var i=0;i<tokens.length;i++)//parcurg lista de tpkeni
        {
            tn = '${'+ tokens[i].tokenName + '}';
            input = input.replace('...', tn);//inlocuiesc pe rand prima aparitie a '...' cu token-ul curent
        }
    }
    else
    {
        //input nu contine '...'
    }

    return input; 
}

//functie folosita la verificarea formatului token-urilor
function verifyTokenFormat(tokens){

      for (var i=0;i<tokens.length;i++){
            if( typeof tokens[i].tokenName != 'string' )
                return false;
      }

      return true;
}

const app = {
    addTokens: addTokens
   
    
}

module.exports = app;